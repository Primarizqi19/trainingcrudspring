package com.springcrud.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.springcrud.model.Orang;

@Repository
public class OrangDaoImpl extends AbstractDao implements OrangDao{

	@SuppressWarnings("unchecked")
	public List<Orang> getAll() {
		// TODO Auto-generated method stub
		List<Orang> item = null;
		
		String sql = "From Orang";
		
		try {			
			item = new ArrayList<Orang>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(sql);
			item = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}finally {
			getSession().close();
		}
		
		return item;
	}

	public Orang getById(Long id) {
		// TODO Auto-generated method stub
		Orang item = null;
		try {
			item = new Orang();
			getSession().beginTransaction();
			item =   (Orang) getSession().get(Orang.class, id);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("getById orangDaoImpl :" +e);
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
		return item;
	}

	public void delete(Orang entity) {
		// TODO Auto-generated method stub
		try {
			getSession().beginTransaction();
			getSession().delete(entity);
		
		
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void update(Orang update) {
		// TODO Auto-generated method stub
		
		try {
			getSession().beginTransaction();
			getSession().update(update);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void save(Orang entity) {
		// TODO Auto-generated method stub
		
		try {			
			getSession().beginTransaction();
			getSession().save(entity);
		} catch (Exception e) {
			// TODO: handle exception
			
			System.err.println(e);
			getSession().getTransaction().rollback();
			
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}
}



