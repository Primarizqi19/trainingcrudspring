package com.springcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "orang")
public class Orang {
	@Id
	private long NIK;
	
	@Column
	private String namaOrang;
	
	@OneToMany (mappedBy = "orangs", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private Set<Barang> bar;
	
	public long getNIK() {
		return NIK;
	}

	public void setNIK(long nIK) {
		NIK = nIK;
	}

	public String getNamaOrang() {
		return namaOrang;
	}

	public void setNamaOrang(String namaOrang) {
		this.namaOrang = namaOrang;
	}


	
}
