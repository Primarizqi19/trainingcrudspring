package com.springcrud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.springcrud.model.Orang;

import com.springcrud.service.OrangService;

@Controller
public class OrangController {

	@Autowired
	OrangService orangService;
	// Method Add Person
	@RequestMapping(value = "/addOrang")
	public ModelAndView add(Model model) {
		ModelAndView modelAndView = new ModelAndView("orang-add");

		try {

			Orang item = new Orang();
			
			modelAndView.addObject("item", item);

		} catch (Exception e) {
			System.err.println("Error Add Orang");
		}

		return modelAndView;

	}

	// Method Save Person
	@RequestMapping(value = "/saveOrang", method = RequestMethod.POST)
	public ModelAndView saving(@ModelAttribute Orang item) {
		ModelAndView modelAndView = new ModelAndView("orang-list");

		try {

		if (item != null) {

				orangService.save(item);			

				modelAndView.addObject("item", item);

				System.err.println("after save");
			} else {
				modelAndView.addObject("message", "Data tidak boleh kosong");
			}

		} catch (Exception e) {
			// TODO: handle exception

			System.err.println("controller /saveItem: " + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

	// Method Menampilkan List Person
	@RequestMapping(value = "/orang-list")
	public ModelAndView getAllData() {

		ModelAndView modelAndView = null;
		List<Orang> item = null;
		try {
			item = new ArrayList<Orang>();
			modelAndView = new ModelAndView("orang-list");

			item = orangService.getAll();

			modelAndView.addObject("listOrang", item);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return modelAndView;
	}

	// Method Delete Person
	@RequestMapping(value = "/orang-delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") Long id) {

		try {
			
			orangService.delete(id);

		} catch (Exception e) {
			System.err.println("Error Delete Orang" + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

	// Method Edit Person
	@RequestMapping(value = "/orang-edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {

		ModelAndView modelAndView = new ModelAndView("orang-edit");
		Orang item = null;
		try {

			item = new Orang();
			item = orangService.getById(id);
			modelAndView.addObject("orang", item);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("error reqMap controller orang-edit " + e);
		}
		
		return modelAndView;
	}

	
	//Method Update Person
	@RequestMapping(value = "/orang-update", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute Orang item) {

		ModelAndView modelAndView = new ModelAndView("orang-edit");
		try {

			if (item != null) {
				orangService.update(item);
			}
			modelAndView.addObject("Sukses", "Data berikut berhasil di update");
			modelAndView.addObject("item", item);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("controller / updateOrang:  " + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

}
