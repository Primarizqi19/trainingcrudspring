<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<a href="addOrang">Tambah Data</a>
<br/>
<table>
	<thead>
		<tr>
		<th>NIK</th>
		<th>Nama</th>
		<th>Action</th>		
		</tr>
	
	<c:choose>
		<c:when test="${empty listOrang}">
		No Data
		</c:when>
			<c:otherwise>
				<c:forEach var="item" items="${listOrang}">
				<tr>
					<td>${item.NIK}</td>
					<td>${item.namaOrang}</td>
					<td><a href="orang-edit/${item.NIK}">Edit</a> <a href="orang-delete/${item.NIK}">Delete</a></td>			
				</tr>
				</c:forEach>
			</c:otherwise>
	</c:choose>
	
	</thead>
</table>
</body>
</html>
