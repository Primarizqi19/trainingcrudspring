package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Employees;

public interface EmployeesRepo extends  CrudRepository<Employees, Integer> {

	public Employees findByEmpID(Integer empId);
	public List<Employees> findByEmpName(String empName);
	public List<Employees> findByPosID(Integer posId);
	public List<Employees> findByGender(String gender);
	public List<Employees> findByAge(Integer age);
	public List<Employees> findByGenderAndAge(String gender, Integer age);
}
