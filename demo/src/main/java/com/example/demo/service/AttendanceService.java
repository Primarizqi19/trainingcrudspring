package com.example.demo.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Attendance;

public interface AttendanceService {
	public Attendance getByAttId(@RequestParam Integer attenId);
	public List<Attendance> getByAttenEmpID(@RequestParam Integer empId);
	public List<Attendance> getByAttenDate(@RequestParam Date attDate);
	public List<Attendance> getByAttenStatus(@RequestParam String attStatus);
	public List<Attendance> getAttAll();
	public Map<String, String> setAtt(@RequestBody Attendance a);
	public Map<String, String> deleteAtt(@RequestBody Attendance a);
}
