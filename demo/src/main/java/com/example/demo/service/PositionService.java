package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Position;

public interface PositionService {
	public Position getByPosId(@RequestParam Integer posId);
	public Position getByPosName(@RequestParam String posName);
	public List<Position> getByPosAllowance(@RequestParam Integer allowance);
	public List<Position> getPosAll();
	public Map<String, String> setPos(@RequestBody Position o);
	public Map<String, String> deletePos(@RequestBody Position o);
}
