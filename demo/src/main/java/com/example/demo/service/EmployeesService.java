package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.model.Employees;

public interface EmployeesService {
	public List<Employees> getEmpAll();
	public Map<String, String> setEmp(@RequestBody Employees e);
	public Map<String, String> delEmp(@RequestBody Employees e);
	public List<Employees> getByEmpGenderAndAge(@RequestParam String gender, @RequestParam Integer age);
	public List<Employees> getByEmpAge(@RequestParam Integer age);
	public List<Employees> getByEmpGender(@RequestParam String gender);
	public List<Employees> getByEmpName(@RequestParam String empName);
	public List<Employees> getByEmpPosId(@RequestParam Integer posId);
	public Employees getByEmpId(@RequestParam Integer empId);
}
