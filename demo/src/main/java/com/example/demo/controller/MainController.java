package com.example.demo.controller;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.service.AttendanceService;
import com.example.demo.service.EmployeesService;
import com.example.demo.service.PositionService;

@RestController
public class MainController {

	@Autowired
	private EmployeesService er;
	@Autowired
	private PositionService or;
	@Autowired
	private AttendanceService ar;
	
	//Employees controller
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		return er.getByEmpId(empId);
	}
	
	@GetMapping("/getempposid")
	public List<Employees> getByEmpPosId(@RequestParam Integer posId) {
		return er.getByEmpPosId(posId);
	}
	
	@GetMapping("/getempname")
	public List<Employees> getByEmpName(@RequestParam String empName) {
		return er.getByEmpName(empName);
	}
	
	@GetMapping("/getempgender")
	public List<Employees> getByEmpGender(@RequestParam String gender) {
		return er.getByEmpGender(gender);
	}
	
	@GetMapping("/getempage")
	public List<Employees> getByEmpAge(@RequestParam Integer age) {
		return er.getByEmpAge(age);
	}
	
	@GetMapping("/getempgenderage")
	public List<Employees> getByEmpGenderAndAge(@RequestParam String gender, @RequestParam Integer age) {
		return er.getByEmpGenderAndAge(gender, age);
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		return er.getEmpAll();
	}
	
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e){
		return er.setEmp(e);
	}
	
	@PostMapping("/deleteemp")
	public Map<String, String> delEmp(@RequestBody Employees e){
		return er.delEmp(e);
	}
	
	//Position controller
	
	@GetMapping("/getposid")
	public Position getByPosId(@RequestParam Integer posId) {
		return or.getByPosId(posId);
	}
	
	@GetMapping("/getposname")
	public Position getByPosName(@RequestParam String posName) {
		return or.getByPosName(posName);
	}
	
	@GetMapping("/getposallow")
	public List<Position> getByPosAllowance(@RequestParam Integer allowance) {
		return or.getByPosAllowance(allowance);
	}
	
	@GetMapping("/getposall")
	public List<Position> getPosAll() {
		return or.getPosAll();
	}
	
	@PostMapping("/setpos")
	public Map<String, String> setPos(@RequestBody Position o){
		return or.setPos(o);
	}
	
	@PostMapping("/deletepos")
	public Map<String, String> deletePos(@RequestBody Position o){
		return or.deletePos(o);
	}
	
	//Attendance controller
	
	@GetMapping("/getattenid")
	public Attendance getByAttId(@RequestParam Integer attenId) {
		return ar.getByAttId(attenId);
	}
	
	@GetMapping("/getattenempid")
	public List<Attendance> getByAttenEmpID(@RequestParam Integer empId) {
		return ar.getByAttenEmpID(empId);
	}
	
	@GetMapping("/getattendate")
	public List<Attendance> getByAttenDate(@RequestParam Date attDate) {
		return ar.getByAttenDate(attDate);
	}
	
	@GetMapping("/getattenstatus")
	public List<Attendance> getByAttenStatus(@RequestParam String attStatus) {
		return ar.getByAttenStatus(attStatus);
	}
	
	@GetMapping("/getattenall")
	public List<Attendance> getAttAll() {
		return ar.getAttAll();
	}
	
	@PostMapping("/setatten")
	public Map<String, String> setAtt(@RequestBody Attendance a){
		return ar.setAtt(a);
	}
	
	@PostMapping("/deleteatten")
	public Map<String, String> deleteAtt(@RequestBody Attendance a){
		return ar.deleteAtt(a);
	}
}
