package com.example.demo.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.service.EmployeesService;

@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesService{

	@Autowired
	private EmployeesRepo er;
	
	@Override
	public List<Employees> getEmpAll() {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}

	@Override
	public Map<String, String> setEmp(Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			respMap.put("empID", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception e2) {
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> delEmp(Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			respMap.put("empID", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception e2) {
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

	@Override
	public List<Employees> getByEmpGenderAndAge(String gender, Integer age) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByGenderAndAge(gender, age);
		return empls;
	}

	@Override
	public List<Employees> getByEmpAge(Integer age) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByAge(age);
		return empls;
	}

	@Override
	public List<Employees> getByEmpGender(String gender) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByGender(gender);
		return empls;
	}

	@Override
	public List<Employees> getByEmpName(String empName) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByEmpName(empName);
		return empls;
	}

	@Override
	public List<Employees> getByEmpPosId(Integer posId) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posId);
		return empls;
	}

	@Override
	public Employees getByEmpId(Integer empId) {
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		return employees;
	}


}
