package com.example.demo.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Position;
import com.example.demo.repository.PositionRepo;
import com.example.demo.service.PositionService;

@Service
@Transactional
public class PositionServiceImpl implements PositionService{
	@Autowired
	private PositionRepo or;
	
	@Override
	public Position getByPosId(Integer posId) {
		Position positions = new Position();
		positions = (Position) or.findByPosID(posId);
		return positions;
	}

	@Override
	public Position getByPosName(String posName) {
		Position positions = new Position();
		positions = (Position) or.findByPosName(posName);
		return positions;
	}

	@Override
	public List<Position> getByPosAllowance(Integer allowance) {
		List<Position> positions = new ArrayList<Position>();
		positions = (List<Position>) or.findByAllowance(allowance);
		return positions;
	}

	@Override
	public List<Position> getPosAll() {
		List<Position> positions = new ArrayList<Position>();
		positions = (List<Position>) or.findAll();
		return positions;
	}

	@Override
	public Map<String, String> setPos(Position o) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			o = or.save(o);
			respMap.put("PosID", o.getPosID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception o2) {
			respMap.put("PosID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> deletePos(Position o) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			or.delete(o);
			respMap.put("PosID", o.getPosID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception o2) {
			respMap.put("PosID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

}
