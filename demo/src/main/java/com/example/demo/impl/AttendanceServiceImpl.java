package com.example.demo.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Attendance;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.service.AttendanceService;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService{
	@Autowired
	private AttendanceRepo ar;
	@Override
	public Attendance getByAttId(Integer attenId) {
		Attendance Attendances = new Attendance();
		Attendances = (Attendance) ar.findByAttenId(attenId);
		return Attendances;
	}

	@Override
	public List<Attendance> getByAttenEmpID(Integer empId) {
		List<Attendance> attendances = new ArrayList<Attendance>();
		attendances = (List<Attendance>) ar.findByEmpID(empId);
		return attendances;
	}

	@Override
	public List<Attendance> getByAttenDate(Date attDate) {
		List<Attendance> attendances = new ArrayList<Attendance>();
		attendances = (List<Attendance>) ar.findByAttDate(attDate);
		return attendances;
	}

	@Override
	public List<Attendance> getByAttenStatus(String attStatus) {
		List<Attendance> attendances = new ArrayList<Attendance>();
		attendances = (List<Attendance>) ar.findByAttStatus(attStatus);
		return attendances;
	}

	@Override
	public List<Attendance> getAttAll() {
		List<Attendance> attendances = new ArrayList<Attendance>();
		attendances = (List<Attendance>) ar.findAll();
		return attendances;
	}

	@Override
	public Map<String, String> setAtt(Attendance a) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			a = ar.save(a);
			respMap.put("AttendanceID", a.getAttenId().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception a2) {
			respMap.put("AttendanceID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> deleteAtt(Attendance a) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			ar.delete(a);
			respMap.put("AttendanceID", a.getAttenId().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("esg", "success");
		} catch(Exception a2) {
			respMap.put("AttendanceID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("esg", "failed");
		}
		return respMap;
	}

}
