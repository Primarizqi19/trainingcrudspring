package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;

@RestController
public class MainController {

	@Autowired
	private EmployeesRepo er;
	
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		return employees;
	}
	
}
