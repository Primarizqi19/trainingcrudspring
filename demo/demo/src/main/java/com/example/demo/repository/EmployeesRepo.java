package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Employees;

public interface EmployeesRepo extends  CrudRepository<Employees, Integer> {

	public Employees findByEmpID(Integer empId);
}
